# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2021, 2022, 2024.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-09-04 00:41+0000\n"
"PO-Revision-Date: 2024-09-04 11:52+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 23.04.3\n"

#: inputsequence.cpp:135
#, kde-format
msgctxt "@action:button This action is disabled"
msgid "Disabled"
msgstr "Вимкнено"

#: inputsequence.cpp:138
#, kde-format
msgctxt "@action:button There is no keybinding"
msgid "None"
msgstr "Немає"

#: inputsequence.cpp:145
#, kde-format
msgctxt "@action:button"
msgid "Left mouse button"
msgstr "Ліва кнопка миші"

#: inputsequence.cpp:147
#, kde-format
msgctxt "@action:button"
msgid "Right mouse button"
msgstr "Права кнопка миші"

#: inputsequence.cpp:149
#, kde-format
msgctxt "@action:button"
msgid "Middle mouse button"
msgstr "Середня кнопка миші"

#: inputsequence.cpp:155
#, kde-format
msgctxt "@action:button"
msgid "Application-defined"
msgstr "Визначене програмою"

#: kcmtablet.cpp:34
#, kde-format
msgid "Default"
msgstr "Типова"

#: kcmtablet.cpp:35
#, kde-format
msgid "Portrait"
msgstr "Книжкова"

#: kcmtablet.cpp:36
#, kde-format
msgid "Landscape"
msgstr "Альбомна"

#: kcmtablet.cpp:37
#, kde-format
msgid "Inverted Portrait"
msgstr "Перевернута книжкова"

#: kcmtablet.cpp:38
#, kde-format
msgid "Inverted Landscape"
msgstr "Перевернута альбомна"

#: kcmtablet.cpp:88
#, kde-format
msgid "Follow the Current Screen"
msgstr "Слідувати за поточним екраном"

#: kcmtablet.cpp:93
#, kde-format
msgid "All Screens"
msgstr "Усі екрани"

#: kcmtablet.cpp:102
#, kde-format
msgctxt "model - (x,y widthxheight)"
msgid "%1 - (%2,%3 %4×%5)"
msgstr "%1 – (%2,%3 %4×%5)"

#: kcmtablet.cpp:162
#, kde-format
msgid "Fit to Screen"
msgstr "Вписати в екран"

#: kcmtablet.cpp:163
#, kde-format
msgid "Keep Aspect Ratio and Fit Within Screen"
msgstr "Зберігати пропорції та вписати в екран"

#: kcmtablet.cpp:164
#, kde-format
msgid "Map to Portion of Screen"
msgstr "Прив'язати до частки екрана"

#: ui/ActionDialog.qml:24
#, kde-format
msgctxt "@title Select the action for the tablet button"
msgid "Select Button Action"
msgstr "Вибрати дію для кнопки"

#: ui/ActionDialog.qml:70
#, kde-format
msgctxt "@info %1 is the 'pad button %1' or 'pen button 1/2/3' strings"
msgid "Choose what action will be taken when pressing %1:"
msgstr "Виберіть дію у відповідь на натискання %1:"

#: ui/ActionDialog.qml:84
#, kde-format
msgctxt "@option:radio Set this action to an application-defined type"
msgid "Defer to application"
msgstr "Обробити у програмі"

#: ui/ActionDialog.qml:99
#, kde-format
msgctxt "@option:radio Set this action to a keyboard type"
msgid "Send keyboard key"
msgstr "Надіслати натискання клавіші"

#: ui/ActionDialog.qml:113
#, kde-format
msgctxt "@option:radio Set this action to a mouse type"
msgid "Send mouse button click"
msgstr "Надіслати клацання кнопкою миші"

#: ui/ActionDialog.qml:129
#, kde-format
msgctxt "@option:radio Disable this action"
msgid "Do nothing"
msgstr "Нічого не робити"

#: ui/ActionDialog.qml:149
#, kde-format
msgctxt "@info"
msgid ""
"This tablet button is forwarded to the focused application, which can decide "
"how to handle it."
msgstr ""
"Натискання цієї кнопки на планшеті буде переспрямовано до вікна фокусованої "
"програми, яка вирішить, як обробляти натискання."

#: ui/ActionDialog.qml:177
#, kde-format
msgctxt "@action:inmenu Left mouse click"
msgid "Left-click"
msgstr "Клацання лівою"

#: ui/ActionDialog.qml:178
#, kde-format
msgctxt "@action:inmenu Middle mouse click"
msgid "Middle-click"
msgstr "Клацання середньою"

#: ui/ActionDialog.qml:179
#, kde-format
msgctxt "@action:inmenu Right mouse click"
msgid "Right-click"
msgstr "Клацання правою"

#: ui/ActionDialog.qml:206
#, kde-format
msgctxt "@option:check The ctrl modifier on the keyboard"
msgid "Ctrl"
msgstr "Ctrl"

#: ui/ActionDialog.qml:213
#, kde-format
msgctxt "@option:check The alt modifier on the keyboard"
msgid "Alt"
msgstr "Alt"

#: ui/ActionDialog.qml:220
#, kde-format
msgctxt "@option:check The meta modifier on the keyboard"
msgid "Meta"
msgstr "Meta"

#: ui/ActionDialog.qml:227
#, kde-format
msgctxt "@option:check The shift modifier on the keyboard"
msgid "Shift"
msgstr "Shift"

#: ui/ActionDialog.qml:235
#, kde-format
msgctxt "@info"
msgid "This button is disabled and will not do anything."
msgstr "Цю кнопку вимкнено, її натискання не призводитиме до жодної реакції."

#: ui/Calibration.qml:31
#, kde-format
msgctxt "@title"
msgid "Pen Calibration"
msgstr "Калібрування пера"

#: ui/Calibration.qml:85
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Calibration is completed and saved.<nl/><nl/>Refine the calibration further "
"or close the window."
msgstr ""
"Калібрування завершено, і дані збережено.<nl/><nl/>Можете продовжити "
"удосконалювати калібрування або закрити це вікно."

#: ui/Calibration.qml:86
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Tap the center of each target.<nl/><nl/>Aim for the point where the stylus "
"tip lands, and ignore the cursor position on the screen."
msgstr ""
"Натисніть на центр кожної цілі.<nl/><nl/>Цільтеся у точку, у яку потрапляє "
"кінчик стила, ігноруйте розташування вказівника на екрані."

#: ui/Calibration.qml:92
#, kde-format
msgctxt "@action:button"
msgid "Refine Existing Calibration"
msgstr "Удосконалити наявне калібрування"

#: ui/Calibration.qml:101
#, kde-format
msgctxt "@action:button"
msgid "Calibrate from Scratch"
msgstr "Калібрувати з початку"

#: ui/Calibration.qml:115
#, kde-format
msgctxt "@action:button"
msgid "Close"
msgstr "Закрити"

#: ui/Calibration.qml:115
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr "Скасувати"

#: ui/main.qml:32
#, kde-format
msgid "No drawing tablets found"
msgstr "Планшетів для малювання не знайдено"

#: ui/main.qml:33
#, kde-format
msgid "Connect a drawing tablet"
msgstr "Встановлення з'єднання з планшетом для малювання"

#: ui/main.qml:41
#, kde-format
msgctxt "Tests tablet functionality like the pen"
msgid "Test Tablet…"
msgstr "Перевірка планшета…"

#: ui/main.qml:67
#, kde-format
msgctxt "@label:listbox The device we are configuring"
msgid "Device:"
msgstr "Пристрій:"

#: ui/main.qml:105
#, kde-format
msgid "Map to screen:"
msgstr "Прив’язка до екрана:"

#: ui/main.qml:125
#, kde-format
msgid "Orientation:"
msgstr "Орієнтація:"

#: ui/main.qml:131
#, kde-format
msgid "Not Supported"
msgstr "Підтримки не передбачено"

#: ui/main.qml:138
#, kde-format
msgid "Left-handed mode:"
msgstr "Режим шульги:"

#: ui/main.qml:149
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Tells the device to accommodate left-handed users. Effects will vary by "
"device, but often it reverses the pad buttonsʼ functionality so the tablet "
"can be used upside-down."
msgstr ""
"Повідомляє пристрою, що слід підлаштуватися під шульгу. Результат залежить "
"від пристрою, але часто це інверсія функцій кнопок панелі, щоб планшетом "
"можна було користуватися у перевернутому стані."

#: ui/main.qml:155
#, kde-format
msgid "Mapped Area:"
msgstr "Область прив'язки:"

#: ui/main.qml:242
#, kde-format
msgid "Resize the tablet area"
msgstr "Змінити розміри області планшета"

#: ui/main.qml:266
#, kde-format
msgctxt "@option:check"
msgid "Lock aspect ratio"
msgstr "Зафіксувати співвідношення розмірів"

#: ui/main.qml:274
#, kde-format
msgctxt "tablet area position - size"
msgid "%1,%2 - %3×%4"
msgstr "%1,%2 – %3×%4"

#: ui/main.qml:285
#, kde-format
msgid "Pen button 1:"
msgstr "Кнопка пера 1:"

#: ui/main.qml:286
#, kde-format
msgctxt ""
"@info Meant to be inserted into an existing sentence like 'configuring pen "
"button 1'"
msgid "pen button 1"
msgstr "кнопка пера 1"

#: ui/main.qml:290
#, kde-format
msgid "Pen button 2:"
msgstr "Кнопка пера 2:"

#: ui/main.qml:291
#, kde-format
msgctxt ""
"@info Meant to be inserted into an existing sentence like 'configuring pen "
"button 2'"
msgid "pen button 2"
msgstr "кнопка пера 2"

#: ui/main.qml:295
#, kde-format
msgid "Pen button 3:"
msgstr "Кнопка пера 3:"

#: ui/main.qml:296
#, kde-format
msgctxt ""
"@info Meant to be inserted into an existing sentence like 'configuring pen "
"button 3'"
msgid "pen button 3"
msgstr "кнопка пера 3"

#: ui/main.qml:351
#, kde-format
msgctxt "@action:button Calibration in progress"
msgid "Calibration in Progress"
msgstr "Виконуємо калібрування"

#: ui/main.qml:353
#, kde-format
msgctxt "@action:button Calibrate the pen display"
msgid "Calibrate"
msgstr "Калібрувати"

#: ui/main.qml:356
#, kde-format
msgctxt "@action:button Pen display doesn't support calibration"
msgid "Calibration Not Supported"
msgstr "Підтримки калібрування не передбачено"

#: ui/main.qml:383
#, kde-format
msgctxt "@info:tooltip"
msgid "Calibration across multiple screens is not supported."
msgstr "Підтримки калібрування між декількома екранами не передбачено."

#: ui/main.qml:393
#, kde-format
msgctxt "@label:listbox The pad we are configuring"
msgid "Pad:"
msgstr "Панель:"

#: ui/main.qml:404
#, kde-format
msgid "None"
msgstr "Немає"

#: ui/main.qml:429
#, kde-format
msgid "Pad button %1:"
msgstr "Кнопка панелі %1:"

#: ui/main.qml:432
#, kde-format
msgctxt ""
"@info Meant to be inserted into an existing sentence like 'configuring pad "
"button 0'"
msgid "pad button %1"
msgstr "кнопка панелі %1"

#: ui/Tester.qml:25
#, kde-format
msgctxt "@title"
msgid "Tablet Tester"
msgstr "Тестування планшета"

#: ui/Tester.qml:48
#, kde-format
msgid "Stylus press X=%1 Y=%2"
msgstr "Стило натиснуто у X=%1 Y=%2"

#: ui/Tester.qml:58
#, kde-format
msgid "Stylus release X=%1 Y=%2"
msgstr "Стило відпущено у X=%1 Y=%2"

#: ui/Tester.qml:68
#, kde-format
msgid "Stylus move X=%1 Y=%2"
msgstr "Стило пересунуто до X=%1 Y=%2"

#: ui/Tester.qml:199
#, kde-format
msgid ""
"## Legend:\n"
"# X, Y - event coordinate\n"
msgstr ""
"## Умовні позначення:\n"
"# X, Y - координати події\n"

#: ui/Tester.qml:206
#, kde-format
msgctxt "Clear the event log"
msgid "Clear"
msgstr "Очистити"

#~ msgid "Primary (default)"
#~ msgstr "Основна (типова)"

#~ msgid "Fit to Output"
#~ msgstr "За виведенням"

#~ msgid "Fit Output in tablet"
#~ msgstr "За виведенням на планшеті"

#~ msgid "Custom size"
#~ msgstr "Нетиповий розмір"

#~ msgid "Target display:"
#~ msgstr "Дисплей призначення:"

#~ msgid "Area:"
#~ msgstr "Область:"

#~ msgid "Tool Button 1"
#~ msgstr "Кнопка на панелі інструментів 1"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Юрій Чорноіван"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "yurchor@ukr.net"
